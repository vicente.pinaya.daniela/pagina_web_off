import { Component, OnInit } from '@angular/core';
//SERVICIOS
import { ProductosService } from 'src/app/servicios/productos.service';
// RUTA
import { Router } from '@angular/router';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})


export class ProductosComponent implements OnInit {

  productos:any[]=[];

  constructor(private _productosService:ProductosService,
               private router:Router) {
                console.log('constructor');
                
    }

  ngOnInit(): void {
    console.log("ngOnInit");
    this.productos = this._productosService.getProductos();
    console.log(this.productos);
  }

  verProducto(idx:number):void {
    console.log(idx);
    this.router.navigate(['/producto',idx]);
    
  }


  // if  producto.categoria

}
