import { Component, OnInit } from '@angular/core';
//RUTA
import { ActivatedRoute } from '@angular/router';
//SERVICIO
import { ProductosService } from '../../servicios/productos.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

 producto:any = {};
  constructor(private activatedRoute: ActivatedRoute,
              private productosService: ProductosService) {
    this.activatedRoute.params.subscribe(params =>{
      console.log(params['id']);

      this.producto = this.productosService.getProducto(params['id']);
    });
   }

  ngOnInit(): void {
  }

}
