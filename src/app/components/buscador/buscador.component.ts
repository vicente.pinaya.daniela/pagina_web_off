import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductosService}from '../../servicios/productos.service';



@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  productos:any[]=[];
  termino!: string;

  constructor(private activatedRoute:ActivatedRoute,
              private router: Router,
             private productosService:ProductosService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params =>{
      console.log(params['termino']);
      this.termino = params['termino'];

      this.productos = this.productosService.buscarProductos(this.termino);
      console.log(this.productos);
      
    });
  }

 

  verProducto(idx:number){
    console.log(idx);
    this.router.navigate(['/producto',idx]);
    
  }


}
