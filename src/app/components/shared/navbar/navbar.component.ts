import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {



//hamburguesa
  alerta: string = 'alerta exitosa';
  propiedades: any = {
    visible: false
  }

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  
buscarProducto(termino:string):void {
  console.log(termino);

  this.router.navigate(['/buscar', termino]);
  
}
 
}
