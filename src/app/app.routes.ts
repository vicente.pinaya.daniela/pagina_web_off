import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";
import { ProductosComponent } from "./components/productos/productos.component";

//ruta de producto individual 
import { ProductoComponent } from "./components/producto/producto.component";
import { ContactosComponent } from "./components/contactos/contactos.component";
import { BuscadorComponent } from "./components/buscador/buscador.component";
import { NosotrosComponent } from "./components/nosotros/nosotros.component";


const APP_ROUTES:Routes = [
    { path:'inicio', component:InicioComponent},
    { path: 'nosotros', component: NosotrosComponent},
    { path: 'productos', component: ProductosComponent},
    { path: 'producto/:id', component: ProductoComponent},
    { path: 'contactos', component: ContactosComponent},
    { path: 'buscar/:termino', component:BuscadorComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES)