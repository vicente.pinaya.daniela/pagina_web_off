import { Injectable } from "@angular/core";

@Injectable()
export class ProductosService{


    private productos: Producto[] = [
        {
            nombre: "COLGATE PROTECCION ANTICARIES 180 GRS",
            precio: "Bs. 12.5",
            img: "assets/img/colNormal.jpg",
            aparicion: "1941-11-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",
            
            casa:"DC"

          },
          {
            nombre: "COLGATE CEPILLO KIDS +5 AÑOS",
            precio: "Bs. 7",
            img: "assets/img/colCellKid.jpg",
            aparicion: "1939-05-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            
            casa:"DC"
          },
          {
            nombre: "COLGATE PASTA KIDS FRESA 50 GRS 09A",
            precio: "Bs.10 ",
            img: "assets/img/colKid.jpg",
            aparicion: "1964-01-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            casa: "Marvel"
          },
          {
            nombre: "COLGATE PASTA SMILES JUNIOR 6 AÑOS 75ML",
            precio: "Bs. 15",
            img: "assets/img/colKidBat.jpg",
            aparicion: "1962-05-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            casa:"Marvel"
          },
          {
            nombre: "COLGATE CEPILLO MINIONS 6+ AÑOS x2",
            precio: "Bs. 12",
            img: "assets/img/collCellKidMini.jpg",
            aparicion: "1940-06-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            casa: "DC"
          },
          {
            nombre: "COLGATE CEPILLO 360 SORUND MEDIO X2 3049",
            precio: "Bs. 20",
            img: "assets/img/colSor.jpg",
            aparicion: "1962-08-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",


            casa: "Marvel"
          },
          {
            nombre: "COLGATE PASTA TRIPLE ACCION 180 GRS",
            precio: "Bs. 16",
            img: "assets/img/colTriple.jpg",
            aparicion: "1974-11-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            casa: "Marvel"
          },
          {
            nombre: "CONDOR CEPILLO DENTAL JUNIOR AVENGERS",
            precio: "Bs. 17",
            img: "assets/img/CondorKid.jpg",
            aparicion: "1974-11-01",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

            casa: "Marvel"
          },
          {
            nombre: "CONDOR CEPILLO DENTAL JUNIOR AVENGERS",
            precio: "Bs. 17",
            img: "assets/img/CondorKid.jpg",
            aparicion: "1974-11-01",
            casa: "Marvel",
            descripcion:"Su fórmula con fluoruro activo penetra en sus dientes para retener el calcio natural haciéndolos tan fuertes como puedan ser.algunas de sus carcateristicas son que: Ayuda a combatir la caries incluso entre los dientes, fortalece y limpia los dientes, delicioso sabor refrescante lorem lorem lorem Dientes fuertes y una agradable sensación de frescura.La Pasta Dental Colgate Triple Acción fortalece los dientes, combate la caries por la protección del flúor y tiene un delicioso sabor a menta, dejando una sensación refrescante en la boca.",

          }
    ];

  

    


    constructor(){
        console.log("servicio listo para usarse");
        
    }

    getProductos():any []{
        return this.productos;
       
    }

    getProducto(idx:number): Producto {
        return this.productos[idx];
    }
    

    buscarProductos(termino:string){
      let productosArr: Producto[] = [];

      termino = termino.toLocaleLowerCase();

      for (let i = 0;i < this.productos.length;i ++){
        let producto = this.productos[i];
        let nombre = producto.nombre.toLocaleLowerCase();
          //si encuentra el elemento devuleve 0 o mayor a 0, si no lo encuentra devuleve -1
      if(nombre.indexOf(termino) >= 0){
        producto.idx = i ;
        productosArr.push(producto);
      }


      }
      return productosArr;
    }

    
  }

export interface Producto {
    nombre: string,
    precio: string,
    img :string,
    aparicion: string,
    casa: string,
    descripcion: string,
    idx?: number
}